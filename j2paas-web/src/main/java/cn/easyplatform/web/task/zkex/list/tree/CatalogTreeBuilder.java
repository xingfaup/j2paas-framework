/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.tree;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.ListPagingRequestMessage;
import cn.easyplatform.messages.response.ListPagingResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListPagingVo;
import cn.easyplatform.messages.vos.datalist.ListRecurVo;
import cn.easyplatform.messages.vos.datalist.ListVo;
import cn.easyplatform.spi.service.ListService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.ext.zul.Datalist;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Components;
import org.zkoss.zul.*;
import org.zkoss.zul.event.ZulEvents;

import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CatalogTreeBuilder extends AbstractOperableTreeBuilder {
    /**
     * @param mainTaskHandler
     * @param dl
     */
    public CatalogTreeBuilder(OperableHandler mainTaskHandler, Datalist dl, Component anchor) {
        super(mainTaskHandler, dl, anchor);
    }

    protected Component createContent() {
        Borderlayout layout = new Borderlayout();
        if (getEntity().isShowPanel() && !Strings.isBlank(this.layout.getPanel())) {
            North north = new North();
            north.setHflex("1");
            north.setVflex("1");
            north.setBorder("none");
            if (this.layout instanceof ListRecurVo)
                north.appendChild(buildPanel(this.layout.getPanel(),
                        ((ListRecurVo) this.layout).getPanelQueryInfo()));
            else
                north.appendChild(buildPanel(this.layout.getPanel(), null));
            layout.appendChild(north);
        }
        if (getEntity().getPageSize() > 0) {// 有分页
            if (getEntity().isFetchAll()) {
                listExt.setMold("paging");
                listExt.setPageSize(getEntity().getPageSize());
                listExt.getPaginal().setPageSize(listExt.getPageSize());
                paging = listExt.getPagingChild();
                paging.setAutohide(getEntity().isPagingAutohide());
                paging.setDetailed(getEntity().isPagingDetailed());
                if (getEntity().getPagingMold() != null)
                    paging.setMold(getEntity().getPagingMold());
                if (getEntity().getPagingStyle() != null)
                    paging.setStyle(getEntity().getPagingStyle());
            } else {
                South south = new South();
                south.setHflex("1");
                south.setBorder("none");
                south.appendChild(buildPaging(this.layout));
                layout.appendChild(south);
            }
        }
        Center center = new Center();
        center.setHflex("1");
        center.setVflex("1");
        center.setBorder("none");
        layout.appendChild(center);
        if (!Strings.isBlank(listExt.getHeight()))
            layout.setHeight(listExt.getHeight());
        else if (!Strings.isBlank(listExt.getVflex()))
            layout.setVflex(listExt.getVflex());
        else
            layout.setVflex("1");
        if (!Strings.isBlank(listExt.getWidth()))
            layout.setWidth(listExt.getWidth());
        else if (!Strings.isBlank(listExt.getHflex()))
            layout.setHflex(listExt.getHflex());
        else
            layout.setHflex("1");
        if (Strings.isBlank(listExt.getWidth()))
            listExt.setHflex("1");
        if (Strings.isBlank(listExt.getHeight()))
            listExt.setVflex("1");
        Components.replace(getEntity(), layout);
        listExt.setParent(center);
        return layout;
    }

    protected Component buildPaging(ListVo lv) {
        paging = new Paging();
        if (getEntity().isShowPaging()) {
            if (lv.getData() == null || lv.getData().isEmpty())
                paging.setTotalSize(0);
            else
                paging.setTotalSize(lv.getTotalSize());
            paging.setPageSize(getEntity().getPageSize());
            paging.setDetailed(getEntity().isPagingDetailed());
            paging.setAutohide(getEntity().isPagingAutohide());
            paging.setHflex("1");
            paging.setVflex("1");
            if (getEntity().getPagingMold() != null)
                paging.setMold(getEntity().getPagingMold());
            if (getEntity().getPagingStyle() != null)
                paging.setStyle(getEntity().getPagingStyle());
            paging.setEvent("paging()");
            if (lv instanceof ListRecurVo) {
                int activePage = ((ListRecurVo) lv).getStartPageNo();
                if (activePage > 0)
                    paging.setActivePage(activePage - 1);
            }
            PageUtils.addEventListener(this, ZulEvents.ON_PAGING, paging);
        } else
            paging.setVisible(false);
        return paging;
    }

    @Override
    public void paging(int pageNo) {
        ListService dls = ServiceLocator
                .lookup(ListService.class);
        ListPagingRequestMessage req = new ListPagingRequestMessage(getId(),
                new ListPagingVo(listExt.getId(), pageNo));
        IResponseMessage<?> resp = dls.doPaging(req);
        if (!resp.isSuccess()) {
            MessageBox.showMessage(resp.getCode(), (String) resp.getBody());
        } else {
            clear();
            List<ListRowVo> data = ((ListPagingResponseMessage) resp).getBody();
            redraw(data);
        }
    }

    @Override
    protected void setPagingInfo(int totalSize, int activePageNo) {
        if (paging != null) {
            if (totalSize >= 0)
                paging.setTotalSize(totalSize);
            paging.setActivePage(activePageNo);
        }
    }
}
