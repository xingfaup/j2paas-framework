/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.engine.cmd.application.*;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.*;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.spi.service.ApplicationService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.UserType;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.subject.Subject;

import java.util.Date;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ApplicationServiceImpl extends EngineService implements
        ApplicationService {


    @Override
    public IResponseMessage<?> init(SimpleRequestMessage req) {
        return commandExecutor.execute(new InitCmd(req));
    }

    @Override
    public IResponseMessage<?> ttl(SimpleRequestMessage req) {
        if (req.getSessionId() != null) {
            try {
                Subject subject = new Subject.Builder().sessionId(
                        req.getSessionId()).buildSubject();
                Session session = subject.getSession();
                UserDo user = (UserDo) session
                        .getAttribute(CommandContext.USER_KEY);
                if (user == null) {
                    subject.logout();
                    return new SimpleResponseMessage("E000", null);
                }
                if (user.getType() < UserType.TYPE_SUPER && !subject.isAuthenticated()) {
                    subject.logout();
                    return new SimpleResponseMessage("E000", null);
                }
                user.setLastAccessTime(new Date());
                session.setAttribute(CommandContext.USER_KEY, user);
                session.touch();
            } catch (UnknownSessionException e) {
                return new SimpleResponseMessage("E000", null);
            }
        }
        return new SimpleResponseMessage();
    }

    @Override
    public IResponseMessage<?> getRuntime(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetRuntimeCmd(req));
    }

    @Override
    public IResponseMessage<?> purge(SimpleRequestMessage req) {
        return commandExecutor.execute(new PurgeCmd(req));
    }

    @Override
    public IResponseMessage<?> debug(DebugRequestMessage req) {
        return commandExecutor.execute(new DebugCmd(req));
    }

    @Override
    public IResponseMessage<?> executeExpression(ExpressionRequestMessage req) {
        return commandExecutor.execute(new ExecuteExpressionCmd(req));
    }

    @Override
    public IResponseMessage<?> switchOrg(SwitchOrgRequestMessage req) {
        return commandExecutor.execute(new SwitchOrgCmd(req));
    }

    @Override
    public IResponseMessage<?> i18n(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new I18nCmd(req));
    }

    @Override
    public IResponseMessage<?> getAcc(GetAccRequestMessage req) {
        return commandExecutor.execute(new GetAccCmd(req));
    }

    @Override
    public IResponseMessage<?> getWizard(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new GetWizardCmd(req));
    }

    @Override
    public IResponseMessage<?> getConfig(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new GetConfigCmd(req));
    }
}
