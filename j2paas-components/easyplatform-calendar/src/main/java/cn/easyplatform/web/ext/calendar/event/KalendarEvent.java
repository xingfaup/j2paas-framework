/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.event;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

import java.util.Date;

/**
 * Created by shiny on 2017-04-26.
 */
public class KalendarEvent extends Event {
    private Date[] dates;
    private String title;
    private String color;
    private boolean edit;

    public KalendarEvent(String name, Component target) {
        super(name, target);
    }

    public void setDate(Date[] date) {
        this.dates = date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public Date[] getDate() {
        return this.dates;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }
}
