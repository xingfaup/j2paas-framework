/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.entities.beans.api.ApiBean;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.admin.ApiVo;
import cn.easyplatform.messages.vos.admin.ResourceVo;
import cn.easyplatform.services.IDataSource;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.spi.extension.ApplicationService;
import cn.easyplatform.services.SystemServiceId;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ServiceType;
import cn.easyplatform.type.StateType;
import cn.easyplatform.util.EntityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetDataCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public GetDataCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        int type = (int) req.getBody();
        IProjectService ps = cc.getProjectService();
        if (type == ServiceType.DATASOURCE) {
            // 数据源
            List<ResourceVo> data = new ArrayList<>();
            if (ps == null) {
                IDataSource ds = (IDataSource) cc.getEngineConfiguration().getService(SystemServiceId.SYS_ENTITY_DS);
                ResourceVo rv = (ResourceVo) EntityUtils.entity2Vo(ds.getEntity());
                data.add(rv);
            } else {
                List<EntityInfo> ds = cc.getEntityDao().getModels(ps.getId(), EntityType.DATASOURCE.getName());
                for (EntityInfo entity : ds) {
                    entity.setType(EntityType.DATASOURCE.getName());
                    ResourceBean rb = TransformerFactory.newInstance().transformFromXml(entity);
                    ResourceVo rv = (ResourceVo) EntityUtils.entity2Vo(rb);
                    ApplicationService s = cc.getProjectService().getService(entity.getId());
                    if (s != null) {
                        rv.setState(s.getState());
                        rv.setStartTime(s.getStartTime());
                    } else
                        rv.setState(StateType.STOP);
                    data.add(rv);
                }
            }
            return new SimpleResponseMessage(data);
        } else if (type == ServiceType.JOB) {
            // 后作任务
            return new SimpleResponseMessage(ps.getScheduleHandler().getServices());
        } else if (type == ServiceType.API) {
            List<ApiBean> data = cc.getProjectService().getEntityHandler().getEntities(EntityType.API.getName(), null);
            List<ApiVo> list = new ArrayList<>(data.size());
            for (ApiBean ab : data)
                list.add((ApiVo) EntityUtils.entity2Vo(ab));
            return new SimpleResponseMessage(list);
        }
        return new SimpleResponseMessage();

    }
}
