/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.support;

import cn.easyplatform.web.ext.echarts.lib.data.MarkData;
import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class Mark extends Animation implements Serializable {

    /**
     * 图形是否不响应和触发鼠标事件，默认为 false，即响应和触发鼠标事件
     */
    private Boolean silent;
    /**
     * 标注的文本
     */
    private LabelStyle label;
    /**
     * 标线的高亮样式。
     */
    private Emphasis emphasis;

    private List<Object> data;

    public Emphasis emphasis() {
        if (label == null)
            this.emphasis = new Emphasis();
        return emphasis;
    }

    public Mark emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Boolean silent() {
        return silent;
    }

    public Mark silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            this.label = new LabelStyle();
        return label;
    }

    public Mark label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public List<Object> data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public Mark data(Object... values) {
        if (values.length == 0)
            return this;
        if (data == null)
            data = new ArrayList<Object>();
        for (Object v : values) {
            if (v instanceof Map) {
                Map no = (Map) v;
                Iterator<Map.Entry<Object, Object>> itr = no.entrySet().iterator();
                while (itr.hasNext()) {
                    Map.Entry<Object, Object> entry = itr.next();
                    MarkData md = new MarkData();
                    if (entry.getKey().equals("xAxis"))
                        md.xAxis(entry.getValue());
                    else if (entry.getKey().equals("type"))
                        md.type(entry.getValue());
                    else if (entry.getKey().equals("valueIndex"))
                        md.type(entry.getValue());
                    else if (entry.getKey().equals("valueDim"))
                        md.type(entry.getValue());
                    else if (entry.getKey().equals("coord"))
                        md.coord(entry.getValue());
                    else if (entry.getKey().equals("x"))
                        md.x(entry.getValue());
                    else if (entry.getKey().equals("y"))
                        md.y(entry.getValue());
                    else if (entry.getKey().equals("value"))
                        md.value(entry.getValue());
                    else if (entry.getKey().equals("xAxis"))
                        md.xAxis(entry.getValue());
                    else if (entry.getKey().equals("yAxis"))
                        md.yAxis(entry.getValue());
                    if (entry.getKey().equals("xAxis") || entry.getKey().equals("yAxis")) {
                        if (data.isEmpty() && values.length == 2)
                            data.add(new ArrayList<Object>());
                        if (values.length == 2)
                            ((List<Object>) data.get(0)).add(md);
                        else
                            data.add(md);
                    } else
                        this.data.add(md);
                }
            } else
                this.data.add(v);
        }
        return this;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public Emphasis getEmphasis() {
        return emphasis;
    }

    public void setEmphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
    }
}
