/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.batch;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.engine.executor.ExecutorFactory;
import cn.easyplatform.engine.runtime.ComponentProcessFactory;
import cn.easyplatform.engine.runtime.RuntimeTask;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.batch.BatchBean;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.response.BatchResponseMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.BatchVo;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.DeviceType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class BatchTask implements RuntimeTask {

    @Override
    public IResponseMessage<?> doTask(CommandContext cc, BaseEntity bean) {
        WorkflowContext ctx = cc.getWorkflowContext();
        ctx.setParameter("815", false);
        EventLogic el = RuntimeUtils.castTo(cc, ctx.getRecord(),
                ctx.getParameter("818"));
        if (el != null) {
            ctx.getRecord().setParameter("808", Constants.ON_INIT);
            RecordContext[] rcs = ctx.getMappingRecord();
            String result = RuntimeUtils.eval(cc, el, rcs);
            if (!result.equals("0000"))
                return MessageUtils.byErrorCode(cc, ctx.getRecord(),
                        ctx.getId(), result);
        }
        if (cc.getUser().getDeviceType() == DeviceType.AJAX) {
            ExecutorFactory.createBatchExecutor().exec(cc, (BatchBean) bean);
            ctx.setParameter("816", true);// 不管如何，Batch类型都必须是可显示的
            ctx.setParameter("830", bean.getType());
            BatchVo bv = new BatchVo();
            bv.setId(ctx.getId());
            bv.setTitile(ctx.getParameterAsString("811"));
            bv.setImage(ctx.getParameterAsString("813"));
            bv.setOpenModel(ctx.getParameterAsInt("805"));
            return new BatchResponseMessage(bv);
        } else {
            ComponentProcessFactory.createComponentProcess(bean).doComponent(
                    cc, cc, bean);
            return new SimpleResponseMessage();
        }
    }

    @Override
    public IResponseMessage<?> doNext(CommandContext cc,
                                      Map<String, Object> data) {
        return new SimpleResponseMessage();
    }

    @Override
    public IResponseMessage<?> doPrev(CommandContext cc) {
        return new SimpleResponseMessage();
    }

}
