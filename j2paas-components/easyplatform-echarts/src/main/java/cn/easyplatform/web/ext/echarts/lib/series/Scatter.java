/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Scatter extends Series {
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Integer polarIndex;
    private Integer geoIndex;
    private Boolean hoverAnimation;
    private Boolean legendHoverLink;
    private Object symbol;
    private Object symbolSize;
    private Double symbolRotate;
    private Object symbolOffset;
    private Boolean large;
    private Integer largeThreshold;
    private Integer calendarIndex;
    /**
     * 渐进式渲染时每一帧绘制图形数量，设为 0 时不启用渐进式渲染，支持每个系列单独配置
     */
    private Object progressive;
    /**
     * 启用渐进式渲染的图形数量阈值，在单个系列的图形数量超过该阈值时启用渐进式渲染。
     */
    private Object progressiveThreshold;
    /**
     * 如果 symbol 是 path:// 的形式，是否在缩放时保持该图形的长宽比
     */
    private Boolean symbolKeepAspect;

    public Scatter() {
        this.type = "scatter";
    }

    public Boolean symbolKeepAspect() {
        return this.symbolKeepAspect;
    }

    public Scatter symbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
        return this;
    }

    public Object progressiveThreshold() {
        return progressiveThreshold;
    }

    public Scatter progressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
        return this;
    }

    public Object progressive() {
        return progressive;
    }

    public Scatter progressive(Object progressive) {
        this.progressive = progressive;
        return this;
    }

    public Integer calendarIndex() {
        return this.calendarIndex;
    }

    public Scatter calendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public Scatter coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Integer xAxisIndex() {
        return xAxisIndex;
    }

    public Scatter xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Integer yAxisIndex() {
        return yAxisIndex;
    }

    public Scatter yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Integer polarIndex() {
        return polarIndex;
    }

    public Scatter polarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
        return this;
    }

    public Integer geoIndex() {
        return geoIndex;
    }

    public Scatter geoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
        return this;
    }

    public Object symbol() {
        return symbol;
    }

    public Scatter symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return symbolSize;
    }

    public Scatter symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Double symbolRotate() {
        return symbolRotate;
    }

    public Scatter symbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return symbolOffset;
    }

    public Scatter symbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public Boolean hoverAnimation() {
        return hoverAnimation;
    }

    public Scatter hoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
        return this;
    }

    public Boolean legendHoverLink() {
        return legendHoverLink;
    }

    public Scatter legendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
        return this;
    }

    public Boolean large() {
        return large;
    }

    public Scatter large(Boolean large) {
        this.large = large;
        return this;
    }

    public Integer largeThreshold() {
        return largeThreshold;
    }

    public Scatter largeThreshold(Integer largeThreshold) {
        this.largeThreshold = largeThreshold;
        return this;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Integer getPolarIndex() {
        return polarIndex;
    }

    public void setPolarIndex(Integer polarIndex) {
        this.polarIndex = polarIndex;
    }

    public Integer getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
    }

    public Boolean getHoverAnimation() {
        return hoverAnimation;
    }

    public void setHoverAnimation(Boolean hoverAnimation) {
        this.hoverAnimation = hoverAnimation;
    }

    public Boolean getLegendHoverLink() {
        return legendHoverLink;
    }

    public void setLegendHoverLink(Boolean legendHoverLink) {
        this.legendHoverLink = legendHoverLink;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Double getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public Boolean getLarge() {
        return large;
    }

    public void setLarge(Boolean large) {
        this.large = large;
    }

    public Integer getLargeThreshold() {
        return largeThreshold;
    }

    public void setLargeThreshold(Integer largeThreshold) {
        this.largeThreshold = largeThreshold;
    }

    public Integer getCalendarIndex() {
        return calendarIndex;
    }

    public void setCalendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
    }

    public Object getProgressive() {
        return progressive;
    }

    public void setProgressive(Object progressive) {
        this.progressive = progressive;
    }

    public Object getProgressiveThreshold() {
        return progressiveThreshold;
    }

    public void setProgressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
    }

    public Boolean getSymbolKeepAspect() {
        return symbolKeepAspect;
    }

    public void setSymbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
    }
}
