/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.executor;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.engine.executor.batch.BatchExecutor;
import cn.easyplatform.engine.executor.jasper.JasperReportExecutor;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.beans.batch.BatchBean;
import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.type.SubType;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class ExecutorFactory {

	private ExecutorFactory() {
	}

	public final static IExecutor<JasperReportBean> createReportExecutor(
			BaseEntity base) {
		if (base.getSubType().equals(SubType.JASPER.getName()))
			return new JasperReportExecutor();
		throw new EasyPlatformWithLabelKeyException("report.type.invalid",
				base.getId(), base.getSubType());
	}

	public final static IExecutor<BatchBean> createBatchExecutor() {
		return new BatchExecutor();
	}
}
