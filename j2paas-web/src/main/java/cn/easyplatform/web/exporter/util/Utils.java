/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.util;

import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Row;
import org.zkoss.zul.Treecol;
import org.zkoss.zul.impl.HeadersElement;
import org.zkoss.zul.impl.MeshElement;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Utils {
	private Utils() {
	};

	public static MeshElement getTarget(Component cmp) {
		MeshElement target = (MeshElement) invokeComponentGetter(cmp,
				"getListbox", "getGrid", "getTree");
		if (target == null)
			throw new IllegalArgumentException(cmp
					+ " cannot find tagret (MeshElement)");
		return target;
	}

	public static Component getFooters(Component target) {
		return (Component) invokeComponentGetter(target, "getFoot",
				"getListfoot", "getTreefoot");
	}

	public static Component getFooterColumnHeader(Component footer) {
		return (Component) invokeComponentGetter(footer, "getColumn",
				"getListheader", "getTreecol");
	}

	public static String getStringValue(Component component) {
		if (component.getFirstChild() != null
				&& !(component instanceof Treecol))
			component = component.getFirstChild();
		return PageUtils.getComponentLabel(component);
		// return (String)invokeComponentGetter(component, "getLabel",
		// "getText", "getValue");
	}

	public static String getAlign(Component cmp) {
		return (String) invokeComponentGetter(cmp, "getAlign");
	}

	public static int getHeaderSize(Component target) {
		Component headers = getHeaders(target);
		if (headers != null) {
			return headers.getChildren().size();
		}

		List<Component> children = target.getChildren();
		int size = 0;
		for (Component cmp : children) {
			if (cmp instanceof Listitem || cmp instanceof Row) {
				size = Math.max(size, cmp.getChildren().size());
			}
		}
		return size;
	}

	public static Component getHeaders(Component target) {
		for (Component cmp : target.getChildren()) {
			if (cmp instanceof HeadersElement) {
				return cmp;
			}
		}
		return null;
	}

	public static Object invokeComponentGetter(Component target,
			String... methods) {
		Class<? extends Component> cls = target.getClass();
		for (String methodName : methods) {
			try {
				Method method = cls.getMethod(methodName, (Class<?>[]) null);
				Object ret = method.invoke(target, (Object[]) null);
				if (ret != null)
					return ret;
			} catch (SecurityException e) {
			} catch (NoSuchMethodException e) {
			} catch (IllegalArgumentException e) {
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			}
		}
		return null;
	}
}
