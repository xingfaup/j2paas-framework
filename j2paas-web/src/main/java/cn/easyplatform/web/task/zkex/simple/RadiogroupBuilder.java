/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.zul.RadiogroupExt;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.*;

import java.io.IOException;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RadiogroupBuilder extends AbstractQueryBuilder<RadiogroupExt> implements Widget {

    public RadiogroupBuilder(OperableHandler mainTaskHandler, RadiogroupExt comp) {
        super(mainTaskHandler, comp);
    }

    public RadiogroupBuilder(ListSupport support, RadiogroupExt comp, Component anchor) {
        super(support, comp, anchor);
    }

    @Override
    public Component build() {
        PageUtils.checkAccess(main.getAccess(), me);
        me.setAttribute("$proxy", this);
        if (me.isImmediate())
            load();
        PageUtils.processEventHandler(main, me, anchor);
        return me;
    }

    protected void createModel(List<?> data) {
        Component c = null;
        Component parent = null;
        if (me.getCols() > 0) {
            Grid grid = new Grid();
            grid.setOddRowSclass("none");
            if (Strings.isBlank(me.getSclass()))
                grid.setSclass("z-form");
            else
                grid.setSclass("z-form " + me.getSclass());
            me.appendChild(grid);
            Rows rows = new Rows();
            grid.appendChild(rows);
            c = rows;
        } else {
            c = new Hlayout();
            me.appendChild(c);
            parent = c;
        }
        int index = 0;
        boolean hasEmptyLabel = !Strings.isEmpty(me.getEmptyLabel());
        if (hasEmptyLabel) {
            index = 1;
            Radio cbi = new Radio();
            cbi.setStyle(me.getItemStyle());
            cbi.setValue(me.getEmptyValue());
            cbi.setLabel(me.getEmptyLabel());
            if (parent == null) {
                parent = new Row();
                c.appendChild(parent);
            }
            cbi.setDisabled(me.isDisabled());
            parent.appendChild(cbi);
        }
        int size = hasEmptyLabel ? data.size() + 1 : data.size();
        for (; index < size; index++) {
            Object[] fvs = (Object[]) data.get(hasEmptyLabel ? index - 1
                    : index);
            if (me.getCols() > 0 && index % me.getCols() == 0) {
                parent = new Row();
                c.appendChild(parent);
            }
            boolean isRaw = !(fvs[0] instanceof FieldVo);
            Object val = isRaw ? fvs[0] : ((FieldVo) fvs[0]).getValue();
            Radio cbi = new Radio();
            cbi.setStyle(me.getItemStyle());
            cbi.setValue(val);
            if (fvs.length == 1) {
                cbi.setLabel(val == null ? "" : val.toString());
            } else if (fvs.length == 2) {
                val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                cbi.setLabel(val == null ? "" : val.toString());
            } else if (fvs.length == 3) {
                val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                cbi.setLabel(val == null ? "" : val.toString());
                val = isRaw ? fvs[2] : ((FieldVo) fvs[2]).getValue();
                if (val != null) {
                    if (val instanceof String) {
                        String str = (String) val;
                        if (str.startsWith("z-icon")
                                || str.startsWith("fa fa-"))
                            cbi.setIconSclass(str);
                        else
                            cbi.setImage(str);
                    } else if (val instanceof byte[]) {
                        try {
                            Image image = new AImage("",
                                    (byte[]) val);
                            cbi.setImageContent(image);
                        } catch (IOException e) {
                        }
                    }
                }
            }// if
            cbi.setDisabled(me.isDisabled());
            parent.appendChild(cbi);
        }
    }

    @Override
    public void reload(Component me) {
        me.getChildren().clear();
        load();
    }
}
