/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.engine.runtime.ComponentProcessFactory;
import cn.easyplatform.engine.runtime.DataListProcess;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.ListQueryRequestMessage;
import cn.easyplatform.messages.response.ListQueryResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListQueryVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class QueryCmd extends AbstractCommand<ListQueryRequestMessage> {

	/**
	 * @param req
	 */
	public QueryCmd(ListQueryRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		ListQueryVo qv = req.getBody();
		ListContext lc = cc.getWorkflowContext().getList(qv.getId());
		if (lc == null)
			return MessageUtils.dataListNotFound(qv.getId());
		DataListProcess dlp = ComponentProcessFactory.createDatalistProcess(lc
				.getType());
		return new ListQueryResponseMessage(dlp.query(cc, qv));
	}

	@Override
	public String getName() {
		return "list.Query";
	}
}
