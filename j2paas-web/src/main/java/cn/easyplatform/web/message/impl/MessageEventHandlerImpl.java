/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.message.impl;

import cn.easyplatform.web.message.entity.Message;
import cn.easyplatform.web.message.MessageEventHandler;

import java.util.Observable;

/**
 * 系统内置应用侦听者
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MessageEventHandlerImpl extends Observable implements MessageEventHandler {

    @Override
    public void onEvent(Message message, long l, boolean b) throws Exception {
        this.setChanged();
        notifyObservers(message);
    }

}
