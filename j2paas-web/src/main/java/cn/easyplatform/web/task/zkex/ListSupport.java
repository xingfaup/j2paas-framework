/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex;

import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.messages.vos.datalist.ListQueryParameterVo;
import cn.easyplatform.messages.vos.datalist.ListVo;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.ext.zul.Datalist;
import cn.easyplatform.web.task.EventSupport;
import cn.easyplatform.web.task.OperableHandler;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.impl.MeshElement;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ListSupport extends ExportSupport, EventSupport {

    String getName();

    MeshElement getComponent();

    void paging(int pageNo);

    void setPageSize(int pageSize);

    void query(Map<String, ListQueryParameterVo> params);

    void sort(String name, boolean isAscending);

    boolean isEditable();

    boolean isCustom();

    List<ListHeaderVo> getHeaders();

    Datalist getEntity();

    void clear();

    ListRowVo getSelectedValue();

    void filter(ListHeaderVo header);

    void print();

    OperableHandler getMainHandler();

    Map<String, Component> getManagedPanelComponents();

    ListVo getLayout();

    boolean validate(Component c);

    void refreshFoot();

    Component getAnchor();
}
