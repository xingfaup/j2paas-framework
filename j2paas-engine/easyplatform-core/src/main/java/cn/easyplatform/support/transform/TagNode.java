/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.support.transform;

import cn.easyplatform.dos.FieldDo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TagNode {

	private String name;

	private String value;

	private String path;

	private List<TagNode> children;

	private TagNode parent;

	private List<FieldDo> record;

	private int countIndex = 1;

	private Map<String, Integer> map;

	public TagNode() {
	}

	/**
	 * @param name
	 */
	public TagNode(String name) {
		this.name = name;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public TagNode setPath(String path) {
		this.path = path;
		return this;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public TagNode setValue(String value) {
		this.value = value;
		return this;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return the parent
	 */
	public TagNode getParent() {
		return parent;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the children
	 */
	public List<TagNode> getChildren() {
		return children;
	}

	/**
	 * @return the record
	 */
	public List<FieldDo> getRecord() {
		return record;
	}

	/**
	 * @param field
	 */
	public void addField(FieldDo field) {
		if (this.record == null)
			this.record = new ArrayList<FieldDo>();
		this.record.add(field);
	}

	/**
	 * @param child
	 */
	public void appendChild(TagNode child) {
		if (children == null) {
			children = new ArrayList<TagNode>();
			map = new HashMap<String, Integer>();
		}
		Integer index = map.get(child.getPath());
		if (index == null) {
			map.put(child.getPath(), 1);
			child.countIndex = 1;
		} else {
			child.countIndex = index + 1;
			map.put(child.getPath(), child.countIndex);
		}
		child.parent = this;
		children.add(child);
	}

	/**
	 * @return
	 */
	public boolean isLeaf() {
		return children == null;
	}

	public int getCountIndex() {
		return countIndex;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (children != null) {
			sb.append("path:").append(path).append("\n");
			sb.append("children:").append(children.size()).append("\n");
			for (TagNode tn : children) {
				sb.append("\t").append(tn).append("\n");
			}
		} else
			sb.append(name).append(":").append(value).append("\n");
		return sb.toString();
	}

}
