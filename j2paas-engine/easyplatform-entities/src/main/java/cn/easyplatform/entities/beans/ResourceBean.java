/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.helper.PropertyAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "resource")
public class ResourceBean extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1386815874158690512L;

	public final static String ENTITY_DATASOURCE_ID = "sys.datasource.entity";

	@XmlElement(name = "properties")
	@XmlJavaTypeAdapter(value = PropertyAdapter.class)
	private Map<String, String> props;

	public Map<String, String> getProps() {
		return props;
	}

	public void setProps(Map<String, String> props) {
		this.props = props;
	}

}
