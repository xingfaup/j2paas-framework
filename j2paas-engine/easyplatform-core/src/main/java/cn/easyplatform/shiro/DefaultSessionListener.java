/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.shiro;

import cn.easyplatform.dos.EnvDo;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.log.LogManager;
import cn.easyplatform.services.IProjectService;
import cn.easyplatform.spi.engine.EngineFactory;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DefaultSessionListener extends SessionListenerAdapter {

    private final static Logger _log = LoggerFactory.getLogger(DefaultSessionListener.class);

    public void onStop(Session session) {
        UserDo user = (UserDo) session.getAttribute(CommandContext.USER_KEY);
        if (user != null)
            stop(session, user);
    }

    @Override
    public void onExpiration(Session session) {
        onStop(session);
    }

    private void stop(Session session, UserDo user) {
        if (_log.isDebugEnabled())
            _log.debug("Stopping the session [{}]-{}", user.getId(), user.getSessionId());
        EnvDo env = (EnvDo) session.getAttribute(CommandContext.USER_ENV);
        IProjectService ps = (IProjectService) EngineFactory.me().getProjectService(env.getId());
        ps.getRowLockProvider(null).clear(user);
        LogManager.stopUser(user);
        if (_log.isDebugEnabled())
            _log.debug("Session has stopped [{}]-{}", user.getId(), user.getSessionId());
    }
}
