/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import cn.easyplatform.type.IResponseMessage;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
class WarningMessageDlg extends AbstractMessageDlg implements
		EventListener<Event> {

	/**
	 * @param resp
	 * @param sourceListener
	 */
	WarningMessageDlg(IResponseMessage<?> resp,
			EventListener<Event> sourceListener) {
		super(resp, sourceListener);
	}

	@Override
	public void show() {
		Messagebox.show(message, code, Messagebox.IGNORE | Messagebox.ABORT,
				Messagebox.EXCLAMATION, this);
	}

	@Override
	public void onEvent(Event event) throws Exception {
		sourceListener.onEvent(new ActionEvent(event, id));
	}

}
