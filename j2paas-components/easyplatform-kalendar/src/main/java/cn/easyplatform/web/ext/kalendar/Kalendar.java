/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.kalendar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.zkoss.lang.Objects;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.web.ext.Assignable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Kalendar extends HtmlBasedComponent implements Assignable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8489741499316917861L;

	static {
		addClientEvent(Kalendar.class, Events.ON_SELECT, CE_IMPORTANT
				| CE_REPEAT_IGNORE | CE_NON_DEFERRABLE);
	}

	/**
	 * 值
	 */
	private Date _value;

	/**
	 * 未用的日期，可以多个，以逗号分隔，格式:"20160810,20160820"
	 */
	private String _unused;

	/**
	 * @return the unused
	 */
	public String getUnused() {
		return _unused;
	}

	/**
	 * @param unused
	 *            the unused to set
	 */
	public void setUnused(String unused) {
		if (!Objects.equals(_unused, unused)) {
			this._unused = unused;
			smartUpdate("unused", _unused);
		}
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setUnused(Date date) {
		if (date == null)
			return;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		if (Strings.isBlank(this._unused))
			this._unused = sdf.format(date);
		else
			this._unused = this._unused + "," + sdf.format(date);
		smartUpdate("unused", _unused);
	}

	public Date getValue() {
		return _value;
	}

	public void setValue(Object value) {
		if (value == null)
			return;
		if (!Objects.equals(_value, value)) {
			this._value = (Date) value;
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(_value);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			this._value = calendar.getTime();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			smartUpdate("value", sdf.format(_value));
		}
	}

	public boolean isChildable() {
		return false;
	}

	protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
			throws java.io.IOException {
		super.renderProperties(renderer);
		if (_value == null){
			_value = new Date();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(_value);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			this._value = calendar.getTime();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		render(renderer, "value", sdf.format(_value));
		if (!Strings.isBlank(_unused))
			render(renderer, "unused", _unused);
	}

	public void service(org.zkoss.zk.au.AuRequest request, boolean everError) {
		String cmd = request.getCommand();
		if (cmd.equals(Events.ON_SELECT)) {
			String[] data = ((String) request.getData().get("val")).split("/");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(_value);
			calendar.set(Integer.parseInt(data[0]),
					Integer.parseInt(data[1]) - 1, Integer.parseInt(data[2]),
					0, 0, 0);
			_value = calendar.getTime();
			Events.postEvent(new Event(Events.ON_CLICK, this, _value));
		} else if (!cmd.equals(Events.ON_CLICK))
			super.service(request, everError);
	}

}
