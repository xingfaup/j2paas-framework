/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetListVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String rid;

    private String query;

    private String filter;

    private int pageSize;

    private String orderBy;

    private int pageNo = 1;

    private List<Object> parameters;

    private boolean getCount;

    private boolean getMetadataIfEmpty;

    public GetListVo(String rid, String query) {
        this.rid = rid;
        this.query = query;
    }

    public GetListVo(){}

    public void setRid(String rid) {
        this.rid = rid;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getRid() {
        return rid;
    }

    public String getQuery() {
        return query;
    }

    /**
     * @param filter the filter to set
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * @return the filter
     */
    public String getFilter() {
        return filter;
    }

    /**
     * @return the pageSize
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * @param pageSize the pageSize to set
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the pageNo
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo the pageNo to set
     */
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * @return the parameters
     */
    public List<Object> getParameters() {
        return parameters;
    }

    /**
     * @param parameter the parameter to set
     */
    public void setParameter(Object parameter) {
        if (parameters == null)
            parameters = new ArrayList<Object>();
        parameters.add(parameter);
    }

    /**
     * @return the getCount
     */
    public boolean isGetCount() {
        return getCount;
    }

    /**
     * @param getCount the getCount to set
     */
    public void setGetCount(boolean getCount) {
        this.getCount = getCount;
    }

    public boolean isGetMetadataIfEmpty() {
        return getMetadataIfEmpty;
    }

    public void setGetMetadataIfEmpty(boolean getMetadataIfEmpty) {
        this.getMetadataIfEmpty = getMetadataIfEmpty;
    }
}
