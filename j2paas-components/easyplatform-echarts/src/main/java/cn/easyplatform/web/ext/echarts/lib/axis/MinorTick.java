/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

import java.io.Serializable;

public class MinorTick implements Serializable {

    /**
     * 默认显示，属性show控制显示与否
     */
    private Boolean show;

    private Integer splitNumber;

    private Integer length;

    private LineStyle lineStyle;
    /**
     * 获取show值
     */
    public Boolean show() {
        return this.show;
    }

    /**
     * 设置show值
     *
     * @param show
     */
    public MinorTick show(Boolean show) {
        this.show = show;
        return this;
    }


    /**
     * 获取length值
     */
    public Integer length() {
        return this.length;
    }

    /**
     * 设置length值
     *
     * @param length
     */
    public MinorTick length(Integer length) {
        this.length = length;
        return this;
    }
    /**
     * 获取splitNumber值
     */
    public Integer splitNumber() {
        return this.splitNumber;
    }

    /**
     * 设置splitNumber值
     *
     * @param splitNumber
     */
    public MinorTick splitNumber(Integer splitNumber) {
        this.splitNumber = splitNumber;
        return this;
    }
    /**
     * 属性lineStyle控制线条样式，（详见lineStyle）
     *
     * @see LineStyle
     */
    public LineStyle lineStyle() {
        if (this.lineStyle == null) {
            this.lineStyle = new LineStyle();
        }
        return this.lineStyle;
    }

    /**
     * 设置style值
     *
     * @param style
     */
    public MinorTick lineStyle(LineStyle style) {
        this.lineStyle = style;
        return this;
    }
}
