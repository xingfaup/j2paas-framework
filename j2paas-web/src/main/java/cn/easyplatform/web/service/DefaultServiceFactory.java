/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.service;

import cn.easyplatform.aop.ClassAgent;
import cn.easyplatform.aop.ClassDefiner;
import cn.easyplatform.aop.DefaultClassDefiner;
import cn.easyplatform.aop.ServiceInterceptor;
import cn.easyplatform.aop.asm.AsmClassAgent;
import cn.easyplatform.aop.matcher.MethodMatcherFactory;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.spi.service.*;
import cn.easyplatform.web.WebApps;
import cn.easyplatform.web.service.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DefaultServiceFactory implements ServiceFactory {

    private final static Logger log = LoggerFactory.getLogger(DefaultServiceFactory.class);

    private final Map<String, Object> serviceCache = new HashMap<>();

    public DefaultServiceFactory() {
        //注册默认本地的服务
        registry(AddonService.class, AddonServiceImpl.class);
        registry(AdminService.class, AdminServiceImpl.class);
        registry(ApplicationService.class, ApplicationServiceImpl.class);
        registry(ComponentService.class, ComponentServiceImpl.class);
        registry(ListService.class, ListServiceImpl.class);
        registry(IdentityService.class, IdentityServiceImpl.class);
        registry(TaskService.class, TaskServiceImpl.class);
        registry(VfsService.class, VfsServiceImpl.class);
        registry(ApiService.class, ApiServiceImpl.class);
    }

    @Override
    public void registry(Class<?> ifc, Class<?> impl) {
        try {
            if (log.isDebugEnabled())
                log.debug("Find service classes {}", impl);
            ClassDefiner classDefiner = new DefaultClassDefiner(
                    WebApps.class.getClassLoader());
            ClassAgent classAgent = new AsmClassAgent();
            classAgent.addInterceptor(MethodMatcherFactory.matcher(),
                    new ServiceInterceptor());
            Class<?> cl = classAgent.define(classDefiner, impl);
            serviceCache.put(ifc.getSimpleName(), cl.newInstance());
            if (log.isDebugEnabled())
                log.debug("Add service {} {}", ifc, cl);
        } catch (Exception e) {
            throw Lang.wrapThrow(e);
        }
    }

    @Override
    public <T> T lockup(Class<T> clazz) {
        return clazz.cast(serviceCache.get(clazz.getSimpleName()));
    }

    @Override
    public void destory() {
        serviceCache.clear();
    }
}
