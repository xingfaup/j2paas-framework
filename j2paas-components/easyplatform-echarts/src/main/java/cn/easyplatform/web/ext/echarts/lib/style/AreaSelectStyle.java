/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

/**
 * 在坐标轴上可以进行框选，这里是一些框选的设置
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AreaSelectStyle {
    /**
     * 框选范围的宽度
     */
    private Integer width;
    /**
     * 选框的边框宽度
     */
    private Integer borderWidth;
    /**
     * 选框的边框颜色
     */
    private String borderColor;
    /**
     * 选框的填充色
     */
    private String color;
    /**
     * 选框的透明度
     */
    private Double opacity;

    public Integer width() {
        return this.width;
    }

    public AreaSelectStyle width(Integer width) {
        this.width = width;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public AreaSelectStyle borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public String borderColor() {
        return this.borderColor;
    }

    public AreaSelectStyle borderColor(String borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public String color() {
        return this.color;
    }

    public AreaSelectStyle color(String color) {
        this.color = color;
        return this;
    }

    public Double opacity() {
        return this.opacity;
    }

    public AreaSelectStyle opacity(Double opacity) {
        this.opacity = opacity;
        return this;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getOpacity() {
        return opacity;
    }

    public void setOpacity(Double opacity) {
        this.opacity = opacity;
    }
}
