
carousel.Carousel = zk.$extends(zul.Widget, {
    _img: null,
    _title: null,
    _content: null,
    _imgurl: null,
    _imgevt: null,
    _intervalTime:null,
    _query:null,
    _dbId:null,
    _filter:null,

    $define: {
        img: function(val) {
        },
        title: function(val) {
        },
        content: function(val) {
        },
        imgurl: function(val) {
        },
        imgevt: function(val) {
        },
        intervalTime: function(val) {
        },
        query: function(val) {
        },
        dbId: function(val) {
        },
        filter: function(val) {
        },
        reloadFlag: function(val) {
            if(val==true){
                this.rerender();
                this._reloadFlag=false;
            }
        },
    },

    bind_: function () {
        $("#carouselExampleCaptions").carousel({
            interval: 2000 //
        });
        this.$supers(carousel.Carousel,'bind_', arguments);
    },

    unbind_: function () {
        this.$supers(carousel.Carousel,'unbind_', arguments);
    },

    doLink: function (evt) {
        this.fire("onLink",{data:evt});
    },

    _init: function () {

    },

    doClick_: function (evt) {

    }
});