/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao.dialect;

import cn.easyplatform.dao.Page;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class Dialect {

	/**
	 * 根据提供的sql、分页参数：start、pagesize获取分页后的sql语句
	 * 
	 * @param sql
	 *            未分页sql语句
	 * @param pageNo
	 *            当前页数
	 * @param pageSize
	 *            每页记录数
	 * @return
	 */
	public String getPageSql(String sql, Page page) {
		StringBuilder sb = new StringBuilder(sql.length() + 100);
		sb.append(getPageBefore(page));
		sb.append(sql);
		sb.append(getPageAfter(page));
		return sb.toString();
	}

	/**
	 * 根据提供的sql、分页参数：start、pagesize获取分页sql语句before字符串
	 * 
	 * @param pageNo
	 *            当前页数
	 * @param pageSize
	 *            每页记录数
	 * @return
	 */
	protected abstract String getPageBefore(Page page);

	/**
	 * 根据提供的sql、分页参数：start、pagesize获取分页sql语句after字符串
	 * 
	 * @param pageNo
	 *            当前页数
	 * @param pageSize
	 *            每页记录数
	 * @return
	 */
	protected abstract String getPageAfter(Page page);
}
