/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder;

import cn.easyplatform.web.ext.echarts.ECharts;
import cn.easyplatform.web.ext.echarts.builder.impl.*;
import cn.easyplatform.web.ext.echarts.stuido.EchartsCallback;
import cn.easyplatform.web.ext.echarts.util.ChartType;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Window;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class EChartsBuilderFactory {

    public final static void showConfig(ECharts charts, EchartsCallback callback, Page page) {
        Map<String, Object> args = new HashMap<>();
        args.put("charts", charts);
        args.put("event", callback);
        Window win = (Window) Executions.createComponents("~./js/echarts/builder/builder.zul", null, args);
        win.setPage(page);
//        win.setPage(charts.page);
        win.setPosition("top,center");
        win.doOverlapped();
    }

    public final static IEChartsBuilder createBuilder(String type) {
        ChartType tp = ChartType.valueOf(type);
        switch (tp) {
            case line:
                return new LineChartsBuilder(type);
            case bar:
                return new BarChartsBuilder(type);
            case pie:
                return new PieChartsBuilder(type);
            case scatter:
                return new ScatterChartsBuilder(type);
            case effectScatter:
                return new EffectScatterChartsBuilder(type);
            case radar:
                return new RadarChartsBuilder(type);
            case tree:
                return new TreeChartsBuilder(type);
            case treemap:
                return new TreemapChartsBuilder(type);
            case sunburst:
                return new SunburstChartsBuilder(type);
            case boxplot:
                return new BoxplotChartsBuilder(type);
            case candlestick:
                return new CandlestickChartsBuilder(type);
            case heatmap:
                return new HeatmapChartsBuilder(type);
            case map:
                return new MapChartsBuilder(type);
            case parallel:
                return new ParallelChartsBuilder(type);
            case lines:
                return new LinesChartsBuilder(type);
            case graph:
                return new GraphChartsBuilder(type);
            case sankey:
                return new SankeyChartsBuilder(type);
            case funnel:
                return new FunnelChartsBuilder(type);
            case gauge:
                return new GaugeChartsBuilder(type);
            case pictorialBar:
                return new PictorialBarChartsBuilder(type);
            case themeRiver:
                return new ThemeRiverChartsBuilder(type);
            case calendar:
                return new CalendarChartsBuilder(type);
            case threeD:
                return new ThreeDChartsBuilder(type);
            default:
                return new CustomChartsBuilder(type);
        }
    }
}
