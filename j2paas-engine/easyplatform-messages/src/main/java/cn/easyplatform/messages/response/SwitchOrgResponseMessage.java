/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.response;

import java.util.List;

import cn.easyplatform.messages.AbstractResponseMessage;
import cn.easyplatform.messages.vos.AgentVo;
import cn.easyplatform.messages.vos.RoleVo;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SwitchOrgResponseMessage extends AbstractResponseMessage<String>{

	private List<RoleVo> roles;
	
	private List<AgentVo> agents;
	
	/**
	 * @param body
	 */
	public SwitchOrgResponseMessage(String body) {
		super(body);
	}

	public List<RoleVo> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleVo> roles) {
		this.roles = roles;
	}

	public List<AgentVo> getAgents() {
		return agents;
	}

	public void setAgents(List<AgentVo> agents) {
		this.agents = agents;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2570582561082740197L;

}
