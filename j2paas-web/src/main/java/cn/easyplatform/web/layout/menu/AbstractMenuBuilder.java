/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.menu;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.*;
import cn.easyplatform.web.layout.IMenuBuilder;
import cn.easyplatform.web.listener.RunTaskListener;
import cn.easyplatform.web.utils.ExtUtils;
import cn.easyplatform.web.utils.PageUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Nav;
import org.zkoss.zkmax.zul.Navbar;
import org.zkoss.zkmax.zul.Navitem;
import org.zkoss.zul.*;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractMenuBuilder<T extends Component> implements IMenuBuilder, EventListener<Event> {

    protected T box;

    public AbstractMenuBuilder(T box) {
        this.box = box;
    }

    @Override
    public void fill(AuthorizationVo av) {
        if (av == null || av.getRoles() == null)
            return;
        Set<Menu> set = new LinkedHashSet<>();
        for (RoleVo rv : av.getRoles()) {
            for (MenuVo mv : rv.getMenus())
                set.add(new Menu(mv, rv, null));
        }
        if (av.getAgents() != null && !av.getAgents().isEmpty()) {
            for (AgentVo agent : av.getAgents()) {
                for (RoleVo rv : agent.getRoles()) {
                    for (MenuVo mv : rv.getMenus())
                        set.add(new Menu(mv, rv, agent));
                }
            }
        }
        for (Menu mv : set)
            createMenu(mv);
    }

    protected abstract void createMenu(Menu menu);

    protected void createPanel(Component parent, Menu mv) {
        West west = new West();
        west.setVflex("1");
        west.setSize("160px");
        west.setParent(parent);
        Caption caption = new Caption();
        caption.setLabel(mv.getMenu().getName());
        caption.setParent(west);
        west.setParent(parent);
        Center center = new Center();
        center.setHflex("1");
        center.setVflex("1");
        center.setBorder("none");
        center.setParent(parent);
        Component container = center;
        String c = (String) box.getAttribute("container");
        if ("mdi".equals(c)) {
            Tabbox tbx = new Tabbox();
            tbx.setHflex("1");
            tbx.setVflex("1");
            tbx.setOrient("bottom");
            tbx.setMaximalHeight(true);
            tbx.setTabscroll(true);
            tbx.setParent(center);
            tbx.appendChild(new Tabs());
            tbx.appendChild(new Tabpanels());
            container = tbx;
        }
        String menubar = (String) box.getAttribute("menubar");
        if ("navbar".equals(menubar)) {
            west.appendChild(createNavbar(container, mv.getRole(), mv.getAgent(), mv.getMenu()));
            A a = new A();
            a.setIconSclass("z-icon-th-large z-borderlayout-icon");
            a.setWidgetAttribute("data-label", mv.getMenu().getName());
            a.setWidgetListener(Events.ON_CLICK, "var n=this.parent.nextSibling;n.setCollapsed(!n.isCollapsed());if(n.isCollapsed()){n.parent.setWidth('26px');this.parent.setLabel('')}else{n.parent.setWidth('160px');this.parent.setLabel(jq(this).attr('data-label'))}");
            a.setParent(caption);
        } else {
            west.appendChild(createTree(container, mv.getRole(), mv.getAgent(), mv.getMenu()));
            west.setBorder("none");
            west.setCollapsible(true);
        }
    }

    private Tree createTree(Component container, RoleVo role, AgentVo agent, MenuVo mv) {
        Tree tree = new Tree();
        tree.setVflex(true);
        tree.setHflex("true");
        Treechildren children = new Treechildren();
        if (tree.getTreechildren() != null) {
            if (!Strings.isBlank(tree.getTreechildren().getSclass()))
                children.setSclass(tree.getTreechildren().getSclass());
            if (!Strings.isBlank(tree.getTreechildren().getStyle()))
                children.setStyle(tree.getTreechildren().getStyle());
        }
        children.setParent(tree);
        if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty()) {
            for (MenuVo vo : mv.getChildMenus()) {
                createMenu(container, role, agent, children, vo);
            }
        }
        if (mv.getTasks() != null && !mv.getTasks().isEmpty()) {
            for (MenuNodeVo tv : mv.getTasks()) {
                createTask(container, role, agent, children, tv);
            }
        }
        if (children.getFirstChild() != null) {
            ((Treeitem) children.getFirstChild()).setOpen(true);
            ((Treecell) children.getFirstChild().getFirstChild()
                    .getFirstChild()).setIconSclass("z-icon-folder-open");
        }
        return tree;
    }

    private void createMenu(Component container, RoleVo role, AgentVo agent,
                            HtmlBasedComponent parent, MenuVo mv) {
        Treeitem ti = new Treeitem();
        ti.setValue(mv);
        ti.setParent(parent);
        ti.setOpen(false);
        Treerow tr = new Treerow();
        tr.setParent(ti);
        Treecell tc = new Treecell(mv.getName());
        tc.setTooltiptext(mv.getName());
        tc.setParent(tr);
        if (!Strings.isBlank(mv.getImage()))
            PageUtils.setTaskIcon(tc, mv.getImage());
        else
            tc.setIconSclass("z-icon-folder");
        ti.setWidgetListener(Events.ON_OPEN, TreeMenuBuilder.NODE_OPEN_SCRIPT);
        ti.setWidgetListener(Events.ON_CLICK, TreeMenuBuilder.NODE_CLICK_SCRIPT);
        Treechildren children = new Treechildren();
        children.setParent(ti);
        if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty()) {
            for (MenuVo vo : mv.getChildMenus()) {
                createMenu(container, role, agent, children, vo);
            }
        }
        if (mv.getTasks() != null && !mv.getTasks().isEmpty()) {
            for (MenuNodeVo tv : mv.getTasks()) {
                createTask(container, role, agent, children, tv);
            }
        }
    }

    private void createTask(Component container, RoleVo role, AgentVo agent,
                            HtmlBasedComponent parent, MenuNodeVo tv) {
        Treeitem ti = new Treeitem();
        tv.setRoleId(role.getId());
        if (agent != null)
            tv.setAgent(agent.getId());
        ti.setParent(parent);
        Treerow tr = new Treerow();
        tr.setParent(ti);
        Treecell tc = new Treecell(tv.getName());
        tc.setTooltiptext(tv.getDesp() == null ? tv.getName() : tv.getDesp());
        tc.setParent(tr);
        PageUtils.setTaskIcon(tc, tv.getImage());
        ti.addEventListener(Events.ON_CLICK, new RunTaskListener(tv, container));
    }

    private Navbar createNavbar(Component container, RoleVo rv, AgentVo av, MenuVo mv) {
        Navbar navbar = new Navbar();
        navbar.setOrient("vertical");
        if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty())
            for (MenuVo cv : mv.getChildMenus())
                createNav(container, navbar, cv, rv, av, 0);
        if (mv.getTasks() != null && !mv.getTasks().isEmpty())
            createTask(container, navbar, mv.getTasks(), rv, av, 0);
        if (navbar.getFirstChild() instanceof Nav)
            ((Nav) navbar.getFirstChild()).setOpen(true);
        return navbar;
    }

    private void createNav(Component container, Component parent, MenuVo mv, RoleVo rv, AgentVo av,
                           int level) {
        Nav nav = new Nav(mv.getName());
        if (!Strings.isBlank(mv.getImage()))
            PageUtils.setTaskIcon(nav, mv.getImage());
        else
            nav.setIconSclass(ExtUtils.getIconSclass());
        if (mv.getChildMenus() != null && !mv.getChildMenus().isEmpty()) {
            for (MenuVo cv : mv.getChildMenus())
                createNav(container, nav, cv, rv, av, level + 1);
        }
        if (mv.getTasks() != null && !mv.getTasks().isEmpty())
            createTask(container, nav, mv.getTasks(), rv, av, level + 1);
        nav.setWidgetAttribute("level", "" + (level == 0 ? 5 : (level * 16)));
        nav.setParent(parent);
    }

    private void createTask(Component container, Component parent, List<MenuNodeVo> tasks,
                            RoleVo rv, AgentVo av, int level) {
        for (MenuNodeVo tv : tasks) {
            tv.setRoleId(rv.getId());
            if (av != null)
                tv.setAgent(av.getId());
            Navitem nv = new Navitem();
            nv.setLabel(tv.getName());
            nv.setTooltiptext(tv.getDesp() == null ? tv.getName() : tv
                    .getDesp());
            PageUtils.setTaskIcon(nv, tv.getImage());
            nv.setParent(parent);
            nv.setWidgetAttribute("level", "" + (level == 0 ? 5 : (level * 16)));
            nv.addEventListener(Events.ON_CLICK, new RunTaskListener(tv, container));
        }
    }

    protected static class Menu {
        private MenuVo mv;
        private RoleVo rv;
        private AgentVo av;

        public Menu(MenuVo mv, RoleVo rv, AgentVo av) {
            this.mv = mv;
            this.rv = rv;
            this.av = av;
        }

        public MenuVo getMenu() {
            return mv;
        }

        public RoleVo getRole() {
            return rv;
        }

        public AgentVo getAgent() {
            return av;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Menu menu = (Menu) o;
            return menu.getMenu().equals(this.getMenu());
        }

        @Override
        public int hashCode() {
            return mv.hashCode();
        }
    }
}
