/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.application.GetFieldsCmd;
import cn.easyplatform.engine.cmd.application.GetSourceCmd;
import cn.easyplatform.engine.cmd.application.GetValueCmd;
import cn.easyplatform.engine.cmd.application.SetValueCmd;
import cn.easyplatform.engine.cmd.task.*;
import cn.easyplatform.messages.request.*;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TaskServiceImpl extends EngineService implements
        TaskService {

    @Override
    public IResponseMessage<?> begin(BeginRequestMessage req) {
        return commandExecutor.execute(new BeginCmd(req));
    }

    @Override
    public IResponseMessage<?> save(SaveRequestMessage req) {
        return commandExecutor.execute(new SaveCmd(req));
    }

    @Override
    public IResponseMessage<?> refresh(RefreshRequestMessage req) {
        return commandExecutor.execute(new RefreshCmd(req));
    }

    @Override
    public IResponseMessage<?> next(NextRequestMessage req) {
        return commandExecutor.execute(new NextCmd(req));
    }

    @Override
    public IResponseMessage<?> prev(SimpleRequestMessage req) {
        return commandExecutor.execute(new PrevCmd(req));
    }

    @Override
    public IResponseMessage<?> close(SimpleRequestMessage req) {
        return commandExecutor.execute(new CloseCmd(req));
    }

    @Override
    public IResponseMessage<?> getFields(GetFieldsRequestMessage req) {
        return commandExecutor.execute(new GetFieldsCmd(req));
    }

    @Override
    public IResponseMessage<?> reload(ReloadRequestMessage req) {
        return commandExecutor.execute(new ReloadCmd(req));
    }

    @Override
    public IResponseMessage<?> getValue(GetValueRequestMessage req) {
        return commandExecutor.execute(new GetValueCmd(req));
    }

    @Override
    public IResponseMessage<?> setValue(SetValueRequestMessage req) {
        return commandExecutor.execute(new SetValueCmd(req));
    }

    @Override
    public IResponseMessage<?> getSource(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetSourceCmd(req));
    }

    @Override
    public IResponseMessage<?> action(ActionRequestMessage req) {
        return commandExecutor.execute(new ActionCmd(req));
    }

    @Override
    public IResponseMessage<?> batch(ListBatchRequestMessage req) {
        return commandExecutor.execute(new BatchCmd(req));
    }

    @Override
    public IResponseMessage<?> subscribe(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new SubscribeCmd(req));
    }

    @Override
    public IResponseMessage<?> unsubscribe(SimpleTextRequestMessage req) {
        return commandExecutor.execute(new UnsubscribeCmd(req));
    }
}
