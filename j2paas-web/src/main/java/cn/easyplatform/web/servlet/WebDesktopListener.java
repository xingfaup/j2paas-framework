/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.servlet;

import cn.easyplatform.web.Lifecycle;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.util.DesktopCleanup;
import org.zkoss.zk.ui.util.DesktopInit;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WebDesktopListener implements DesktopInit, DesktopCleanup {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.zkoss.web.ui.util.DesktopCleanup#cleanup(org.zkoss.web.ui.Desktop)
	 */
	@Override
	public void cleanup(Desktop desktop) throws Exception {
		Lifecycle.endDesktop(desktop);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.zkoss.web.ui.util.DesktopInit#init(org.zkoss.web.ui.Desktop,
	 * java.lang.Object)
	 */
	@Override
	public void init(Desktop desktop, Object request) throws Exception {
		Lifecycle.beginDesktop(desktop);
	}

}
