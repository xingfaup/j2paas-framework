/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.impl;

import java.io.Serializable;
import java.util.Date;

import cn.easyplatform.web.ext.calendar.api.CalendarEvent;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SimpleCalendarEvent implements CalendarEvent, Serializable {
	private static final long serialVersionUID = 20090316143135L;
	private Object id;
	private String _headerColor = "";
	private String _contentColor = "";
	private String _content = "";
	private String _title = "";
	private Date _beginDate;
	private Date _endDate;
	private boolean _locked;

	public Date getBeginDate() {
		return _beginDate;
	}

	public void setBeginDate(Date beginDate) {
		_beginDate = beginDate;
	}

	public Date getEndDate() {
		return _endDate;
	}

	public void setEndDate(Date endDate) {
		_endDate = endDate;
	}

	public String getContent() {
		return _content;
	}

	public void setContent(String content) {
		_content = content;
	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		_title = title;
	}

	public String getContentColor() {
		return _contentColor;
	}

	public void setContentColor(String ccolor) {
		_contentColor = ccolor;
	}

	public String getHeaderColor() {
		return _headerColor;
	}

	public void setHeaderColor(String hcolor) {
		_headerColor = hcolor;
	}

	public String getZclass() {
		return "z-calevent";
	}

	public boolean isLocked() {
		return _locked;
	}

	public void setLocked(boolean locked) {
		_locked = locked;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

}
