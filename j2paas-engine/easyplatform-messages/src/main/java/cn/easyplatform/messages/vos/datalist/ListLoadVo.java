/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import cn.easyplatform.type.FieldVo;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListLoadVo implements Serializable {

    private String id;

    private String query;

    private FieldVo value;

    private boolean count;

    public ListLoadVo(String id, String query, FieldVo value) {
        this(id, query, value, false);
    }

    public ListLoadVo(String id, String query, FieldVo value, boolean count) {
        this.id = id;
        this.query = query;
        this.value = value;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public String getQuery() {
        return query;
    }

    public FieldVo getValue() {
        return value;
    }

    public boolean isCount() {
        return count;
    }
}
